# frozen_string_literal: true

# Inspec test for recipe .::worker

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe command("kubectl version --short | awk '{print $3}'") do
  its('stdout') { should match(/v1.15.\d+/) }
end
