# frozen_string_literal: true

module KubeadmCookbook
  #
  # Kubeadm cookbook methods to configure Haproxy
  #
  module Haproxy
    include Chef::Mixin::ShellOut
    # master_nodes
    include KubeadmCookbook::Finders

    # This method return:
    #
    # * in 'centralized' multi_master mode:
    #   - when called by master nodes: the local HAproxy port
    #   - when called by worker nodes: the remote HAproxy port from the masters
    #
    # * in 'decentralized' multi_master mode:
    #   - when called by master nodes: the kubeapi port of the master node which
    #     has initialized the cluster (/!\ SPOF to be fixed! /!\)
    #   - when called by worker nodes: the local HAproxy port
    def build_haproxy_server_list(options = {})
      kubeapi_port = detect_master_port(as: :master)

      nodes = Array(master_nodes)
      nodes |= [node] if options[:as] == :master

      nodes.map do |master|
        master_ip_address = search_master_ip_address(master)

        "#{master['hostname']} #{master_ip_address}:#{kubeapi_port}"
      end.uniq
    end

    def sysctl_net_ipv4_ip_nonlocal_bind_should_be_update_for?(node)
      case node['platform']
      when 'debian'
        os_release_version = shell_out(
          'cat /etc/os-release | grep "VERSION="'
        ).stdout
        os_version = os_release_version.scan(/VERSION="(\d+)/).flatten.first

        # Debian Buster and greater needs to update
        # the sysctl net.ipv4.ip_nonlocal_bind
        os_version.to_i > 9
      when 'ubuntu'
        true
      end
    end
  end
end
