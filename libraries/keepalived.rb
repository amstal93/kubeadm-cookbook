# frozen_string_literal: true

module KubeadmCookbook
  #
  # Kubeadm cookbook methods to configure Keepalived
  #
  module Keepalived
    # first_valid_master_node
    include KubeadmCookbook::Finders

    def fetch_password
      password = fetch_password_from_another_master_node
      Chef::Log.warn "kubeadm: 1: keepalived password: #{password.inspect}"
      password ||= fetch_keepalived('authentication_password')
      Chef::Log.warn "kubeadm: 2: keepalived password: #{password.inspect}"
      password ||= auto_generate_password
      Chef::Log.warn "kubeadm: 3: keepalived password: #{password.inspect}"

      persist_node_attribute!('authentication_password', password)

      password
    end

    def fetch_vrrp_id
      vrrp_id = fetch_keepalived('virtual_router_id')

      vrrp_id || 1
    end

    private

    # Keepalived password are limited to 8 characters ...
    def auto_generate_password
      Chef::Log.warn 'kubeadm: generating a keepalived password ...'

      (0...8).map { (65 + rand(26)).chr }.join
    end

    def fetch_keepalived(attribute_name)
      if node['kubeadm']['keepalived']
        Chef::Log.warn 'kubeadm: keepalived configured, returning the ' \
                       "#{attribute_name} if any ..."

        node_config = node['kubeadm']['keepalived']
        Chef::Log.warn "kubeadm: keepalived config: #{node_config.inspect}"

        if node_config[attribute_name].nil? ||
           (node_config[attribute_name].is_a?(Integer) == false &&
            node_config[attribute_name].empty?)
          Chef::Log.warn "kubeadm: Node config keepalived #{attribute_name} " \
                         'is blank, returning nil ...'

          nil
        else
          Chef::Log.warn "kubeadm: Node config has a #{attribute_name}, " \
                         'returning it ...'

          node_config[attribute_name]
        end
      else
        Chef::Log.warn 'kubeadm: keepalived is not configured, therefore ' \
                       "no #{attribute_name} can be retrieved, returning nil"
        nil
      end
    end

    def fetch_password_from_another_master_node
      master = first_valid_master_node

      unless master
        Chef::Log.warn 'kubeadm: Cannot retrieve a password from another ' \
                       'master node as no master nodes have been found. ' \
                       'Returning nil.'

        return
      end

      Chef::Log.warn 'kubeadm: Master nodes found, returning password from ' \
                     "master node #{master['hostname']} " \
                     "(IP: #{master['ipaddress']}) ..."

      master_keepalived = master.normal['kubeadm']['keepalived']
      Chef::Log.warn "kubeadm: Master node config: #{master_keepalived.inspect}"

      if master_keepalived.key?('authentication_password')
        if master_keepalived['authentication_password'].nil? ||
           master_keepalived['authentication_password'].empty?
          Chef::Log.warn 'kubeadm: Master node config keepalived password ' \
                         'is blank, returning nil ...'

          nil
        else
          Chef::Log.warn 'kubeadm: Master node config has a keepalived ' \
                         'password, returning it ...'

          master_keepalived['authentication_password']
        end
      else
        Chef::Log.warn "kubeadm: Master node config doesn't have any " \
                       'authentication_password key, returning nil.'
      end
    end

    def persist_node_attribute!(attribute_name, attribute_value)
      Chef::Log.warn "kubeadm: Updating node attribute #{attribute_name} " \
                     "with #{attribute_value.inspect} ..."
      node.normal['kubeadm']['keepalived'][attribute_name] = attribute_value
    end
  end
end
