# frozen_string_literal: true

module KubeadmCookbook
  #
  # Sanity check methods for the kubeadm cookbook.
  #
  module SanityCheck
    # formatted_cni_driver_name
    # formatted_ip_address_version
    include KubeadmCookbook::Formatters

    def allowed_cni_drivers
      %w[flannel calico]
    end

    def allowed_network_address_versions
      %w[ipv4 ipv6]
    end

    def can_t_fetch_kubeconfig!
      Chef::Log.fatal 'kubeadm: Unable to fetch the kubeconfig file from ' \
                      'existing master nodes!'
      ensure_have_a_master_node_log

      raise
    end

    def certificate_key_not_found!
      Chef::Log.fatal 'kubeadm: Unable to fetch the certificate key from a ' \
                      'Kubernetes master.'
      ensure_have_a_master_node_log

      raise
    end

    def discovery_token_ca_cert_hash_not_found!
      Chef::Log.fatal 'kubeadm: Unable to retrieve the discovery token CA ' \
                      'cert Hash from a Kubernetes master.'
      ensure_have_a_master_node_log

      raise
    end

    def ensure_ip_address_information_are_present!
      ensure_node_attrbutes_have_ip_address_version_defined!

      return if node_attributes_network_interface_version_is_valid?

      allowed_versions = allowed_formatted_network_address_versions.join("\n")

      Chef::Log.fatal "kubeadm: #{formatted_ip_address_version} is " \
                      'not a valid value.'
      Chef::Log.fatal 'kubeadm: Allowed network address versions are ' \
                      "#{allowed_versions}."
      Chef::Log.fatal 'kubeadm: Please update the node configuration and run ' \
                      'converge again.'

      raise
    end

    def ensure_have_a_master_node_log
      Chef::Log.fatal 'kubeadm: Please ensure you have a node with the ' \
                      'kubeadm::master recipe, and run converge again.'
    end

    def ensure_kubernetes_cni_driver_is_present!
      unless formatted_cni_driver_name.nil? ||
             formatted_cni_driver_name.to_s.empty?
        return
      end

      Chef::Log.fatal 'kubeadm: The configuration for this node misses the ' \
                      'Kubernetes network driver to be installed.'
      Chef::Log.fatal 'kubeadm: Please update the node configuration ' \
                      'attributes in order to set the `cni_driver` attribute ' \
                      'and run again the node converge.'

      raise
    end

    def ensure_kubernetes_cni_driver_is_valid!
      ensure_kubernetes_cni_driver_is_present!

      return if allowed_cni_drivers.include?(formatted_cni_driver_name)

      Chef::Log.fatal "kubeadm: The driver #{formatted_cni_driver_name} is " \
                      'not a supported driver. The supported drivers are ' \
                      "#{allowed_cni_drivers.join(', ')}"

      raise
    end

    def ensure_node_network_configuration_will_match_attributes!
      ensure_node_attributes_have_interface!
      ensure_node_has_requested_network_interface!
      ensure_ip_address_information_are_present!
    end

    def ensure_node_attributes_have_interface!
      return true if interface

      Chef::Log.fatal 'kubeadm: The configuration for this node misses the ' \
                      'interface, therefore this recipe cannot retrieve the ' \
                      'node IP address to be used.'
      Chef::Log.fatal 'kubeadm: Please update the node configuration ' \
                      'attributes in order to set the `interface` attribute ' \
                      'and run again the node converge.'
      raise
    end

    def ensure_node_has_requested_network_interface!
      return if node['network']['interfaces'].key?(interface)

      Chef::Log.fatal "kubeadm: No #{interface} interface found on the node."
      Chef::Log.fatal "kubeadm: Available interfaces are:\n\n" \
                      "#{node['network']['interfaces'].keys.join("\n")}"
      raise
    end

    def interface
      @interface ||= node['kubeadm']['interface']
    end

    def ip_address_not_found!
      addresses = node['network']['interfaces'][interface]['addresses']

      Chef::Log.fatal 'kubeadm: Unable to find a ' \
                      "#{formatted_ip_address_version} IP address from the " \
                      "network interface #{interface}."
      Chef::Log.fatal 'kubeadm: Here are the network interface addresses: ' \
                      "#{addresses.keys.join("\n")}."
      Chef::Log.fatal 'kubeadm: Please review your node, and update ' \
                      'accordingly the node configuration and run again the ' \
                      'converge action.'

      raise
    end

    def kubernetes_token_not_found!
      Chef::Log.fatal 'kubeadm: Unable to retrieve the Kubernetes token ' \
                      'from a master node.'
      ensure_have_a_master_node_log

      raise
    end

    def master_ip_address_not_found!
      Chef::Log.fatal 'kubeadm: Unable to find a the master node IP address.'
      ensure_have_a_master_node_log

      raise
    end

    def node_attributes_network_interface_version_is_valid?
      allowed_network_address_versions.include?(
        formatted_ip_address_version
      )
    end

    private

    def ensure_node_attrbutes_have_ip_address_version_defined!
      return unless node['kubeadm']['ip_address_version'].nil?
      return unless node['kubeadm']['ip_address_version'].empty?

      Chef::Log.fatal 'kubeadm: Unable to find the node IP address from ' \
                      "the network interface #{interface} as the " \
                      'configuration for this node misses the network ' \
                      'address version (ipv4 or ipv6).'
      Chef::Log.fatal "kubeadm: Please update the node's attributes in order " \
                      'to define the `ip_address_version` and run converge ' \
                      'again.'

      raise
    end
  end
end
