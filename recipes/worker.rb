# frozen_string_literal: true

include_recipe 'kubeadm::common'

# `current_kubernetes_version`
# `upgrading_kubernetes?`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::Helper)

ohai 'reload_network' do
  action :reload
  plugin 'network'
end

ruby_block 'populate run_state role' do
  action :run
  block do
    node.run_state['role'] = 'worker'
  end
end

first_master = first_valid_master_node
puts "first_master['kubeadm']: #{first_master['kubeadm'].inspect}"
case first_master['kubeadm']['multi_master']
when 'off'
  # Nothing to do here, workers know the IP address of the single master.
when 'centralized'
  # keepalived and HAproxy is installed on master nodes only, and worker nodes
  # know the Virtual IP address from keepalived.
when 'decentralized'
  # Installs a local HAproxy configured with the master nodes so that Kubernetes
  # points to that HAproxy in order to access the master nodes.
  include_recipe 'kubeadm::install_decentralized_worker_environment'
when 'balanced'
  # Worker nodes refers to the external load balancer, so no need to install
  # anything.
else
  Chef::Log.fatal 'kubeadm: Unknown multi_master mode ' \
                  "'#{first_master['kubeadm']['multi_master']}'. Please " \
                  'update your configuration and set the kubeadm ' \
                  'multi_master attribute to "off", "centralized" or ' \
                  '"decentralized". (See the kubeadm cookbook documentation).'

  raise
end

ruby_block 'worker action' do
  action :run
  block do
    worker_action = 'install_worker'

    if node.run_state['upgrading_kubernetes']
      #
      # This cookbook support defining upgrade recipe for a specific Kubernetes
      # version.
      #
      # For example, if you would need it for Kubernetes 1.20 you would have to
      # create the file `upgrades/masterworker_upgrade_1-20.rb` and it would be
      # used instead of the general `upgrades/worker_upgrade.rb` file.
      worker_action = "worker_upgrade_#{current_kubernetes_version}"

      unless File.exist?(File.join(__dir__, 'upgrades', worker_action))
        worker_action = 'worker_upgrade'
      end
    end

    run_context.include_recipe "kubeadm::#{worker_action}"
  end
  notifies :reload, 'ohai[reload_network]', :before
end
