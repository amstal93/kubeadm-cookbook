# frozen_string_literal: true

require 'base64'

# `can_t_fetch_kubeconfig!`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::SanityCheck)
# `fetch_kube_config_from_master`
Chef::Recipe.send(:include, KubeadmCookbook::Helper)
# `kubernetes_version_from_attributes` in `ruby_block`
# `stream_command`
# `wait_command_output`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::Helper)
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::StreamCommand)

tmp_kubeconfig_file_path = '$HOME/.kube-config'

ruby_block 'warning about migration' do
  action :run
  block do
    Chef::Log.warn 'kubeadm: Running the Kubernetes worker upgrade to version 1.15 ...'
  end
end

ruby_block 'install kube config file' do
  action :run
  block do
    kube_config = fetch_kube_config_from_master || can_t_fetch_kubeconfig!

    command = shell_out(
      <<~CMD
        echo "#{kube_config}" | \
        base64 -d > #{tmp_kubeconfig_file_path}
      CMD
    )

    unless command.exitstatus.zero?
      Chef::Log.error 'kubeadm: Unable to write kube config file ' \
                      "(exist with #{command.exitstatus}): #{command.stderr}"
      exit 1
    end
  end
end

ruby_block 'kubeadm drain node' do
  action :run
  block do
    command_flags = '--ignore-daemonsets'

    command_flags += if current_kubernetes_version.tr('_', '.').to_f > 1.20
                       ' --delete-emptydir-data'
                     else
                       ' --delete-local-data'
                     end

    command = <<~CMD
      kubectl drain `hostname`
                    #{command_flags}
                    --kubeconfig=#{tmp_kubeconfig_file_path}
    CMD

    node.run_state['node_drained'] = stream_command_and_watch_for(
      command,
      /drained/
    )
  end
end

ruby_block 'kubeadm upgrade node' do
  action :run
  block do
    command = <<~CMD
      kubeadm upgrade node \
          --kubelet-version v#{kubernetes_version_from_attributes} \
          --kubeconfig=#{tmp_kubeconfig_file_path}
    CMD

    node.run_state['node_upraded'] = stream_command_and_watch_for(
      command,
      /node was successfully updated/
    )
  end
  only_if { node.run_state['node_drained'] }
end

package 'kubectl' do
  action %i[unlock install lock]
  version(lazy { build_kubeadm_version })
  only_if { node.run_state['node_upraded'] == true }
end

package 'kubelet' do
  action %i[unlock install lock]
  version(lazy { build_kubeadm_version })
  notifies :restart, 'service[restart kubelet]', :immediately
  only_if { node.run_state['node_upraded'] == true }
end

service 'restart kubelet' do
  service_name 'kubelet'
  supports status: true
  action [:nothing]
end

ruby_block 'kubectl uncordon node' do
  action :run
  block do
    stream_command(
      <<~CMD
        kubectl uncordon `hostname` --kubeconfig=#{tmp_kubeconfig_file_path}
      CMD
    ) do |output|
      Chef::Log.debug "kubeadm: #{output.gsub(/\r\n/, '').gsub(/\t/, '  ')}"
    end
  end
  only_if { node.run_state['node_upraded'] }
end

# Wait for the node to get back to the Ready status
ruby_block 'wait for node to be ready' do
  action :run
  block do
    timeout = 60
    Chef::Log.warn 'kubeadm: Waiting for the node to be Ready (timeout after ' \
                   "#{timeout} seconds) ..."

    command = <<~CMD
      kubectl get nodes --kubeconfig=#{tmp_kubeconfig_file_path} | \
      grep `hostname` | \
      awk '{print $2}'
    CMD

    begin
      # Runs the given command, and look at its ouput if it matches the expected
      # regex until the end of the timeout.
      # Each time the output changes, the block is executed with its output.
      wait_command_output(command, /^Ready$/, timeout: timeout) do |output|
        Chef::Log.warn "kubeadm: Node status changed to #{output}."
      end
    rescue KubeadmCookbook::WaitCommandTimeoutError
      # When the timeout is reached, the WaitCommandTimeoutError is thrown
      Chef::Log.warn 'kubeadm: Waiting for the node to be Ready fails after ' \
                     "#{timeout} seconds."
      Chef::Log.warn "kubeadm: It doesn't mean that the node upgrade failed, " \
                     'but you should check manually the node.'
    end
  end
  only_if { node.run_state['node_upraded'] }
end

ruby_block 'check node Kubernetes version' do
  action :run
  block do
    node_k8s_version = shell_out(
      <<~CMD
        kubectl get nodes --kubeconfig=#{tmp_kubeconfig_file_path} | \
        grep `hostname` | \
        awk '{print $5}'
      CMD
    ).stdout.gsub(/\n/, '')

    Chef::Log.warn 'kubeadm: Node Kubernetes version is ' \
                   "#{node_k8s_version.inspect}"

    if node_k8s_version == "v#{kubernetes_version_from_attributes}"
      Chef::Log.info 'kubeadm: Kubernetes worker upgrade is done'
    else
      Chef::Log.error 'kubeadm: Kubernetes worker upgrade seems to have ' \
                      'failed as the Kubernetes worker version is ' \
                      "#{node_k8s_version} but " \
                      "v#{kubernetes_version_from_attributes} was expected."
    end
  end
  only_if { node.run_state['node_upraded'] }
end

file tmp_kubeconfig_file_path do
  action :delete
  only_if "test -f #{tmp_kubeconfig_file_path}"
end
