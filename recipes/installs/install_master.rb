# frozen_string_literal: true

require 'base64'

# `build_kubeadm_flags_from`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::KubeadmFlags)
# `detect_master_port`
# `detect_node_ip_address`
# `fetch_certificate_key_from_master`
# `fetch_discovery_token_ca_cert_hash_from_master`
# `fetch_token_from_master`
# `kubernetes_master_not_reachable?`
# `kubernetes_master_reachable?`
Chef::Recipe.send(:include, KubeadmCookbook::Helper)
# `formatted_cni_driver_name`
Chef::Recipe.send(:include, KubeadmCookbook::Formatters)
# `certificate_key_not_found!`
# `discovery_token_ca_cert_hash_not_found!`
# `ip_address_not_found!`
# `kubernetes_token_not_found!`
Chef::Recipe.send(:include, KubeadmCookbook::SanityCheck)
# `ensure_node_network_configuration_will_match_attributes!`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::SanityCheck)
# `fetch_existing_master_node_ip_address`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::Helper)
# `stream_command`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::StreamCommand)

ruby_block 'node preflight checks' do
  action :run
  block do
    ensure_node_network_configuration_will_match_attributes!
  end
  not_if { ::File.exist?('/etc/kubernetes/kubelet.conf') }
end

ruby_block 'fetching node IP address' do
  action :run
  block do
    # Node's VPN IP address
    node.run_state['node_ip_address'] = detect_node_ip_address ||
                                        ip_address_not_found!
  end
end

ruby_block 'node information grabbing' do
  action :run
  block do
    # Node's VPN IP address or Load Balancer one
    node.run_state['master_ip_address'] = if node.run_state['first_control_plane']
                                            detect_node_ip_address(as: :master) ||
                                              ip_address_not_found!
                                          else
                                            search_master_ip_address ||
                                              ip_address_not_found!
                                          end
    # Node's port or Load Balancer's port
    node.run_state['master_port'] = detect_master_port(as: :master)

    kubeadm_action = if node.run_state['first_control_plane']
                       'initializ'
                     else
                       'join'
                     end

    Chef::Log.info 'kubeadm: This node will use the IP Address ' \
                   "#{node.run_state['master_ip_address'].inspect} with " \
                   "kubeadm on port #{node.run_state['master_port']} " \
                   "when #{kubeadm_action}ing the cluster."
  end
  not_if "grep 'https://#{node.run_state['node_ip_address']}' " \
               '/etc/kubernetes/kubelet.conf'
end

log 'inform about node being first master or not' do
  level :warn
  message 'kubeadm: This node is' \
          "#{' not' unless node.run_state['first_control_plane']} " \
          'seen as the cluster master node.'
  not_if { node['kubeadm']['multi_master'] == 'off' }
  not_if "grep 'https://#{node.run_state['node_ip_address']}' " \
               '/etc/kubernetes/kubelet.conf'
end

ruby_block 'fetch token and certificate key' do
  action :run
  block do
    node.run_state['kubeadm_token'] = fetch_token_from_master ||
                                      kubernetes_token_not_found!
    node.run_state['certificate_key'] = fetch_certificate_key_from_master ||
                                        certificate_key_not_found!
  end
  only_if { node.run_state['first_control_plane'] != true }
  not_if do
    kubelet_configured = ::File.exist?('/etc/kubernetes/kubelet.conf') &&
                         File.open('/etc/kubernetes/kubelet.conf')
                             .grep(%r{https://#{node.run_state['node_ip_address']}})

    Chef::Log.warn " ----oo0000-> kubelet_configured: #{kubelet_configured.inspect}"

    kubelet_configured || node.run_state['control_plane_redeploy'] == false
  end
end

ruby_block 'show tokens to be used with the join action' do
  action :run
  block do
    node.run_state['ca_cert_hash'] = begin
      fetch_discovery_token_ca_cert_hash_from_master ||
        discovery_token_ca_cert_hash_not_found!
    end

    Chef::Log.warn 'kubeadm: Using token ' \
                   "#{node.run_state['kubeadm_token'].inspect}, " \
                   "ca cert #{node.run_state['ca_cert_hash'].inspect} and " \
                   'certificate key ' \
                   "#{node.run_state['certificate_key'].inspect} to join " \
                   'the Kubernetes cluster'

    if node.run_state['kubeadm_token'].nil? || node.run_state['kubeadm_token'].empty?
      Chef::Log.fatal 'kubeadm: ERROR: token is blank therefore the join ' \
                      "command can't work."
      Chef::Log.info 'kubeadm: Try to converge again another master node so ' \
                     'that the token is refreshed and then retry to ' \
                     'bootstrap this node.'

      exit 1
    end
  end
  not_if do
    kubelet_configured = ::File.exist?('/etc/kubernetes/kubelet.conf') &&
                         File.open('/etc/kubernetes/kubelet.conf')
                             .grep(%r{https://#{node.run_state['node_ip_address']}})

    kubelet_configured || node.run_state['first_control_plane'] == true ||
      node.run_state['control_plane_redeploy'] == false
  end
end

#
# Before to do anything, we ensure there's a valid CNI driver configured
#
ensure_kubernetes_cni_driver_is_valid!

# Updates the kubelet default configuration
template '/etc/default/kubelet' do
  source 'kubelet.erb'
  owner 'root'
  group 'root'
  mode '0755'
  variables node_ip: lazy { node.run_state['node_ip_address'] }
  not_if "grep '--node-ip=#{lazy { node.run_state['node_ip_address'] }}' " \
               '/etc/default/kubelet'

  notifies :restart, 'service[restart kubelet]', :immediately
end

ruby_block 'generate a certificate key' do
  action :run
  block do
    node_certificate_key = nil

    if node.run_state['first_control_plane']
      Chef::Log.warn 'kubeadm: Generating a new certificate key ...'

      command = 'kubeadm certs certificate-key'
      node_certificate_key = shell_out(command).stdout.gsub(/\r\n/, '')

      Chef::Log.warn "kubeadm: Generated key is #{node_certificate_key}"
    else
      Chef::Log.warn 'kubeadm: Saving certificate key from first control ' \
                     'plane in this secondary control plane ...'
      node_certificate_key = node.run_state['certificate_key']
    end

    node.normal['kubeadm']['certificate_key'] = node_certificate_key

    Chef::Log.warn 'kubeadm: Saved certificate key is ' \
                   "#{node['kubeadm']['certificate_key']}"
  end
  not_if { node['kubeadm']['multi_master'] == 'off' }
  only_if do
    node.run_state['first_control_plane'] ||
      node['kubeadm']['certificate_key'].to_s.empty?
  end
end

#
# Generates the kubeadm-config.yaml file which is the most compatible option
# across kubeadm versions.
#
template 'kubeadm-config.yaml' do
  source 'kubeadm-config.yaml.erb'
  variables certificate_key: lazy { node['kubeadm']['certificate_key'] },
            cluster_name: node['kubeadm']['cluster_name'],
            extra_cert_sans: node['kubeadm']['extra_cert_sans'],
            fqdn: node['fqdn'],
            hostname: node['hostname'],
            kubeadm_version: node['kubeadm']['version'].to_s,
            master_ip_address: lazy { node.run_state['master_ip_address'] },
            master_local_port: node['kubeadm']['kubeapi_port'],
            master_port: lazy { node.run_state['master_port'] },
            node_ip_address: lazy { node.run_state['node_ip_address'] },
            pod_cidr: node['kubeadm']['pod_cidr'],
            service_cidr: node['kubeadm']['service_cidr'],
            service_dns_domain: node['kubeadm']['dns_domain']
  only_if { node.run_state['first_control_plane'] }
end

ruby_block 'kubeadm init/join' do
  action :run
  block do
    additional_flags = {}

    first_control_plane = node.run_state['first_control_plane']

    command = if first_control_plane
                Chef::Log.warn 'kubeadm: Running kubeadm init for the first control plane node ...'
                # kubeadm upload cert flag
                if node['kubeadm']['multi_master'] != 'off'
                  Chef::Log.warn 'kubeadm: [1st] multi_master enabled'

                  additional_flags['--upload-certs'] = ''

                  Chef::Log.warn "kubeadm: [1st] additional_flags: #{additional_flags.inspect}"
                end

                <<-CMD
                  kubeadm init \
                    #{build_kubeadm_flags_from({
                      '--config': 'kubeadm-config.yaml',
                      '--v': 5
                    }.merge(additional_flags))}
                CMD
              else
                Chef::Log.warn 'kubeadm: Running kubeadm join for another control plane node ...'
                # kubeadm join control plan flag
                if node['kubeadm']['multi_master'] != 'off'
                  Chef::Log.warn 'kubeadm: [nst] multi_master enabled'

                  # kubeadm 1.15 and newer flag is named `--control-plane`
                  additional_flags['--control-plane'] = ''

                  Chef::Log.warn "kubeadm: [nst] additional_flags: #{additional_flags.inspect}"
                end

                hash = "sha256:#{node.run_state['ca_cert_hash']}"
                # An existing master IP address
                ip_address = node.run_state['master_ip_address']
                # This node's IP address
                node_ip = node.run_state['node_ip_address']
                # Kube API port (6443 if not changed)
                port = node.run_state['master_port']

                <<-CMD
                  kubeadm join \
                    #{build_kubeadm_flags_from({
                      "#{ip_address}:#{port}": '',
                      '--apiserver-advertise-address': node_ip,
                      '--certificate-key': node.run_state['certificate_key'],
                      '--discovery-token-ca-cert-hash': hash,
                      '--token': node.run_state['kubeadm_token'],
                      '--v': 5
                    }.merge(additional_flags))}
                CMD
              end

    Chef::Log.warn "kubeadm: [#{first_control_plane ? '1' : 'n'}st] " \
                   "command: #{command.inspect}"

    node.run_state['kubeadm_init_done'] = stream_command_and_watch_for(
      command,
      /(Kubernetes [\w-]+ has initialized successfully|This node has joined the cluster)/
    )
  end
  not_if do
    kubelet_configured = ::File.exist?('/etc/kubernetes/kubelet.conf') &&
                         File.open('/etc/kubernetes/kubelet.conf')
                             .grep(%r{https://#{node.run_state['node_ip_address']}})

    kubelet_configured || node.run_state['control_plane_redeploy'] == false
  end
end

# systemd daemon reload after kubelet config file changed
execute 'systemd daemon reload' do
  action :nothing
  command 'systemctl daemon-reload'
  notifies :restart, 'service[restart kubelet]', :immediately
  only_if { node.run_state['kubeadm_init_done'] }
end

# restart kubelet
service 'restart kubelet' do
  service_name 'kubelet'
  supports status: true
  action [:nothing]
end

# Installs admin Kubernetes configuration to the user's home folder
execute 'kube config' do
  command <<-CMD
    mkdir -p $HOME/.kube
    cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    chown $(id -u):$(id -g) $HOME/.kube/config
    chmod 0644 $HOME/.kube/config
  CMD
  only_if 'test -f /etc/kubernetes/admin.conf'
  not_if 'test -f $HOME/.kube/config ' \
         '&& diff $HOME/.kube/config /etc/kubernetes/admin.conf'
end

ruby_block 'save Kube config file' do
  action :run
  block do
    Chef::Log.debug 'Reading file /etc/kubernetes/admin.conf ...'
    kube = File.read('/etc/kubernetes/admin.conf')
    Chef::Log.debug "Readed #{kube.size} bytes ..."
    node_attributes = node['kubeadm'].dup
    node_attributes['kube_config'] = Base64.encode64(kube)
    Chef::Log.debug "node_attributes: #{node_attributes.inspect}"
    node.normal['kubeadm'] = node_attributes
  end
  only_if 'test -f /etc/kubernetes/admin.conf'
end

ruby_block 'save a valide token' do
  action :run
  block do
    generate_new_token = false

    node_kubeadm_token = if node.run_state['first_control_plane']
                           node['kubeadm']['token']
                         else
                           node.run_state['kubeadm_token']
                         end

    if node_kubeadm_token
      Chef::Log.warn "kubeadm: Node has a token, let's check if it is " \
                      'still valid ...'

      cleaned_token = node_kubeadm_token.gsub(/\r\n/, '').gsub(/\n/, '')

      command = <<-CMD
        kubeadm token list |
        grep -v "TOKEN" |
        grep -v "<invalid>" |
        grep authentication |
        grep "#{cleaned_token}"
      CMD
      existing_token = shell_out(command).stdout.gsub(/\r\n/, '')

      if existing_token == ''
        Chef::Log.warn "kubeadm: The node token #{cleaned_token} is no more " \
                       "valid, let's generate a new one."
        generate_new_token = true
      else
        Chef::Log.warn "kubeadm: The node token #{cleaned_token} is still " \
                        'valid, nothing to do.'

        unless node['kubeadm']['token'] == node_kubeadm_token
          Chef::Log.warn 'kubeadm: Saving the kubeadm token ' \
                         "#{node_kubeadm_token.inspect} in this control " \
                         'plane attributes ...'

          node_attributes = node['kubeadm'].dup
          node_attributes['token'] = node_kubeadm_token
          node.normal['kubeadm'] = node_attributes
          Chef::Log.warn 'kubeadm: Saved token is ' \
                         "#{node.normal['kubeadm']['token'].inspect}"
        end
      end
    else
      Chef::Log.warn "kubeadm: Node is missing a kubeadm token, let's " \
                      'check to retrieve one ...'

      command = <<-CMD
        kubeadm token list |
        grep -v "TOKEN" |
        grep -v "<invalid>" |
        grep authentication |
        awk '{print $1}' |
        head -n 1
      CMD
      valid_token = shell_out(command).stdout.gsub(/\n/, '')

      node_attributes = node['kubeadm'].dup
      node_attributes['token'] = valid_token
      node.normal['kubeadm'] = node_attributes
      Chef::Log.warn 'kubeadm: Saved token is ' \
                     "#{node.normal['kubeadm']['token'].inspect}"

      generate_new_token = valid_token == '' || valid_token.nil?
    end

    if generate_new_token
      command = <<-CMD
        kubeadm token list |
        grep -v "TOKEN" |
        grep -v "<invalid>" |
        grep authentication |
        awk '{print $1}' |
        head -n 1
      CMD
      valid_token = shell_out(command).stdout.gsub(/\r\n/, '').gsub(/\n/, '')

      if valid_token == ''
        Chef::Log.warn "kubeadm: No valid token found, let's create a new one."

        # kubeadm token create returns the created token
        valid_token = shell_out('kubeadm token create').stdout
                                                       .gsub(/\r\n/, '')
                                                       .gsub(/\n/, '')

        if valid_token == ''
          Chef::Log.fatal 'kubeadm: Unable to create a new token, this ' \
                          'master node should have a problem, therefore the ' \
                          'cookbook will stop here in order to preserve the ' \
                          'data.'

          exit 1
        end
      end

      node_attributes = node['kubeadm'].dup
      node_attributes['token'] = valid_token
      node.normal['kubeadm'] = node_attributes
      Chef::Log.warn 'kubeadm: Saved token is ' \
                     "#{node.normal['kubeadm']['token'].inspect}"
    else
      Chef::Log.warn 'kubeadm: No need to generate a new token.'
    end
  end
  only_if do
    kubernetes_master_reachable?(node.run_state['node_ip_address'],
                                 node['kubeadm']['kubeapi_port'])
  end
end

ruby_block 'save discovery token ca cert hash' do
  action :run
  block do
    command = <<-CMD
      openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt |
      openssl rsa -pubin -outform der 2>/dev/null |
      openssl dgst -sha256 -hex |
      sed 's/^.* //'
    CMD
    hash = shell_out(command).stdout.gsub(/\r\n/, '')
    if hash
      if hash =~ /[a-z0-9]{64}/
        Chef::Log.info "\nkubeadm: Discovery token CA Cert hash successfully " \
                       "retrieved: #{hash}"
        node_attributes = node['kubeadm'].dup
        node_attributes['discovery_token_ca_cert_hash'] = hash
        node.normal['kubeadm'] = node_attributes
      else
        Chef::Log.fatal "kubeadm: The retrieved hash doesn't look like a " \
                        'valid hash.'
        Chef::Log.fatal 'kubeadm: The hash must match the regex [a-z0-9]{64}'

        raise
      end
    else
      Chef::Log.fatal 'kubeadm: The Kubernetes discovery token CA cert Hash ' \
                      "couldn't be retrieved."
      Chef::Log.fatal 'kubeadm: This value is required in order to allow the ' \
                      'node to join, in a secure way, this master.'
      Chef::Log.fatal 'kubeadm: Please review the converge logs, check your ' \
                      'node, and run again the converge.'

      raise
    end
  end
  only_if do
    kubernetes_master_reachable?(node.run_state['node_ip_address'],
                                 node['kubeadm']['kubeapi_port'])
  end
end

#
# Installs the Kubernetes network driver (CNI)
#

# `formatted_cni_driver_name` is a formatted value from
# default['kubeadm']['cni_driver']['name'].
# So here we are including the recipe file with has the same name as the CNI
# driver name from the attributes (I.e: This will loads the `flannel.rb` file
# when default['kubeadm']['cni_driver']['name'] = 'flannel'.
#
# When in Multi Master mode, install the CNI driver only on the first control
# plane.
# When in Single Master mode, install the CNI driver on the only control plane
if formatted_cni_driver_name
  include_recipe "kubeadm::#{formatted_cni_driver_name}"
end

# Wait for the master node to be Ready
ruby_block 'wait master to be Ready' do
  action :run
  block do
    Chef::Log.warn 'kubeadm: Waiting for the Kubernetes node to be Ready ' \
                   '(timeout after 30 seconds) ...'

    command = "kubectl get nodes | grep $(hostname) | awk '{print $2}'"

    node.run_state['master_ready'] = false

    begin
      # Runs the given command, and look at its ouput if it matches the expected
      # regex until the end of the timeout (30 seconds).
      # Each time the output changes, the block is executed with its output.
      wait_command_output(command, /^Ready/, timeout: 60) do |output|
        Chef::Log.warn "kubeadm: Kubernetes node status changed to #{output}."
      end

      Chef::Log.warn 'kubeadm: Kubernetes node waiter has finished.'

      node.run_state['master_ready'] = true
    rescue KubeadmCookbook::WaitCommandTimeoutError
      # When the timeout is reached, the WaitCommandTimeoutError is thrown
      Chef::Log.warn 'kubeadm: Waiting for the Kubernetes node to be "Ready" ' \
                     'failed after 30 seconds.'
      Chef::Log.warn "kubeadm: It doesn't mean that the deployment failed, " \
                     'but you should check manually on the node status.'
      Chef::Log.warn 'kubeadm: You can have a look at the node status with ' \
                     'the following command:'
      Chef::Log.warn 'kubeadm: kubectl get nodes | grep $(hostname) | ' \
                     "awk '{print $2}'"
    end
  end
  only_if do
    kubernetes_master_reachable?(node.run_state['node_ip_address'],
                                 node['kubeadm']['kubeapi_port'])
  end
  not_if "[ $(kubectl get nodes | grep $(hostname) | awk '{print $2}') " \
          '== ' \
          "'Ready' ]"
end

#
# Enables the Pod Security Policy feature
#
ruby_block 'enable Pod Security Policy' do
  action :run
  block do
    apiserver_filepath = '/etc/kubernetes/manifests/kube-apiserver.yaml'

    with_psp = File.read(apiserver_filepath).gsub(
      /^(\s+- --enable-admission-plugins=NodeRestriction)$/,
      '\1,PodSecurityPolicy'
    )

    File.open(apiserver_filepath, 'w') { |file| file.puts with_psp }

    node.run_state['psp_updated'] = true
  end
  only_if { node['kubeadm']['psp'] == true && node.run_state['master_ready'] }
  not_if "grep '\-\-enable-admission-plugins=NodeRestriction,PodSecurityPolicy' " \
               '/etc/kubernetes/manifests/kube-apiserver.yaml'
end

# Wait for the master node to be Ready
ruby_block 'wait master to be Ready' do
  action :run
  block do
    Chef::Log.warn 'kubeadm: Waiting for the Kubernetes node to be Ready ' \
                   '(timeout after 60 seconds) ...'

    command = "kubectl get nodes | grep $(hostname) | awk '{print $2}'"

    begin
      # Runs the given command, and look at its ouput if it matches the expected
      # regex until the end of the timeout (60 seconds).
      # Each time the output changes, the block is executed with its output.
      wait_command_output(command, /^Ready/, timeout: 60) do |output|
        Chef::Log.warn "kubeadm: Kubernetes node status changed to #{output}."
      end

      Chef::Log.warn 'kubeadm: Kubernetes node waiter has finished.'
    rescue KubeadmCookbook::WaitCommandTimeoutError
      # When the timeout is reached, the WaitCommandTimeoutError is thrown
      Chef::Log.warn 'kubeadm: Waiting for the Kubernetes node to be "Ready" ' \
                     'failed after 60 seconds.'
      Chef::Log.warn "kubeadm: It doesn't mean that the deployment failed, " \
                     'but you should check manually on the node status.'
      Chef::Log.warn 'kubeadm: You can have a look at the node status with ' \
                     'the following command:'
      Chef::Log.warn 'kubeadm: kubectl get nodes | grep $(hostname) | ' \
                     "awk '{print $2}'"
    end
  end
  only_if { node.run_state['psp_updated'] == true }
  only_if do
    kubernetes_master_reachable?(node.run_state['node_ip_address'],
                                 node['kubeadm']['kubeapi_port'])
  end
  not_if "[ $(kubectl get nodes | grep $(hostname) | awk '{print $2}') " \
          '== ' \
          "'Ready' ]"
end

ruby_block 'saves certificate key on disk' do
  action :run
  block do
    File.open('/tmp/k8s-cert-key', 'w') do |file|
      file.write node['kubeadm']['certificate_key']
    end
  end

  only_if { ENV['HOME'] == '/home/vagrant' }
  not_if { node['kubeadm']['certificate_key'].to_s.empty? }
end
