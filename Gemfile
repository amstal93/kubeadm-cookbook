# frozen_string_literal: true

source 'http://rubygems.org'

# Manages a Chef cookbook's dependencies
# https://github.com/berkshelf/berkshelf
gem 'berkshelf', '~> 7.1'
# ChefSpec is a unit testing and resource coverage (code coverage) framework
# for testing Chef cookbooks ChefSpec makes it easy to write examples and
# get fast feedback on cookbook changes without the need for virtual machines or
# cloud servers.
# https://github.com/chefspec/chefspec
gem 'chefspec', '~> 9.2'
# Automatic Ruby code style checking tool. Aims to enforce the community-driven
# Ruby Style Guide.
# https://github.com/rubocop-hq/rubocop
gem 'rubocop'

group :integration do
  # A Test Kitchen Verifier for InSpec
  # https://github.com/inspec/kitchen-inspec
  gem 'kitchen-inspec', '~> 2.2'
  # Test Kitchen is an integration tool for developing and testing
  # infrastructure code and software on isolated target platforms.
  # https://github.com/test-kitchen/test-kitchen
  gem 'test-kitchen', '~> 2.8'
end

group :vagrant do
  # A Test Kitchen Provisioner for Chef Nodes
  # https://github.com/mwrock/kitchen-nodes
  # gem 'kitchen-nodes', '~> 0.9.1'
  # A Vagrant Driver for Test Kitchen.
  # https://github.com/opscode/kitchen-vagrant
  gem 'kitchen-vagrant', '~> 1.7'
  # Since Vagrant 1.1+ is no longer distributed via Gems, vagrant-wrapper
  # allows you to require and interact with the newer package versions via
  # your Gemfile, shell, or Ruby code.
  # It allows the newer packaged version to take precedence even if
  # the older Vagrant gem remains installed.
  # https://github.com/org-binbab/gem-vagrant-wrapper
  gem 'vagrant-wrapper', '~> 2.0.3'
end

group :docker do
  gem 'kitchen-docker'
end

group :dokken do
  gem 'kitchen-dokken'
end
